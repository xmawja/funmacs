;;================== MODELINE ====================;;
;; Enable modeline.el
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))
