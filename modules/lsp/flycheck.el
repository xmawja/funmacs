;;================== FLYCHECK MODE =====================;;
;; Enable flycheck.el
(use-package flycheck
  :straight t
  :init (global-flycheck-mode))
