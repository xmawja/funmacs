;;================== THEMES ==================;;
;; monokai-pro-theme =======
(use-package doom-themes
  :straight t
  :config
  (load-theme 'doom-one t)
  )

;; FONTS 
(set-face-attribute 'default nil :font "Fira Code Retina" :height 120)

